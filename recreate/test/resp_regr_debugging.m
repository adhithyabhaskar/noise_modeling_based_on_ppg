clear;
TR = 0.72;
HPF_f = 0.008;    [filt_b,filt_a] = butter(2,HPF_f*2*TR,'high');    
volDel=20; volDel_from_end=0; NV_0 = 1200;


%% Framewise displacement
    load("3dmaps-temp.mat")
    load("imgcol.mat")
    load('data/105014/MNINonLinear/physio/phys.mat')
    movRegr=load('data/105014/MNINonLinear/Results/Movement_Regressors_dt.txt');        movRegr=[movRegr, movRegr.^2];
    Mov = abs(movRegr(:,7:12));    Mov1 = sum(Mov(:,1:3),2) ;    Mov2 =  50 *  sum(Mov(:,4:6),2) * (2*pi()/360) ;    FD = Mov1 + Mov2;
    FD(1:volDel) = []; movRegr(1:volDel,:) = [];    
    movRegr = filtfilt(filt_b,filt_a,movRegr);
    
    fprintf('Mean FD: %3.2f mm \n', mean(FD))
    
%     figure
%     plot(timeMR, zscore(FD))   
    
    %%  Disentagle resp, movRegr and DVARS
    
%     resp_der = func_RETR_Resp(...)    % Load regressors for RETROICOR    (resp)
    ind_BOLD=find(trig==1);  
    trig(ind_BOLD(1:volDel))=0;    trig(ind_BOLD(end-volDel_from_end+1:end)) = 0;    
    ind_BOLD=find(trig==1);

    resp_der = diff(resp); resp_der = [resp_der(1); resp_der];
    regr_resp = [resp, resp_der, resp.^2, resp_der.^2];
    regr_resp = regr_resp(ind_BOLD,:);

    regr_nuis = [regr_resp, movRegr, GS];    
    regr_nuis = filtfilt(filt_b,filt_a,regr_nuis ) ;
    regr_nuis = [regr_nuis, ones(NV,1)];
    
    parfor vox=1:NVX  
        voxel = img_col(:,vox);                
        B = regr_nuis\voxel;    
        img_col(:,vox) = voxel - regr_nuis*B;        
    end
log_voxel = voxel

    %% Cardiac pulsatility - RETROICOR
    
    M_order = 6 ;
    RETR_CardRegr_cont=RETR_Card_regressors_v2(time,PPGlocs,M_order);
    RETR_CardRegr = RETR_CardRegr_cont(ind_BOLD+round(+0.4*Fs),:);
    RETR_CardRegr = filtfilt(filt_b,filt_a,RETR_CardRegr);
    for j = 1:size(RETR_CardRegr,2)
        voxel = RETR_CardRegr(:,j);
        B = regr_nuis\voxel;
        RETR_CardRegr(:,j) = voxel - regr_nuis*B;
    end
    regr_RETR = [RETR_CardRegr, ones(NV,1)];  
    