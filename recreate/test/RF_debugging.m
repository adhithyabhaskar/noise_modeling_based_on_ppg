clear;
addpath(genpath(pwd))
Fs = 400; Ts = 1/Fs; 
baseDir = 'data/';
subject = ['103818'];
task_list = {'REST1_LR','REST1_RL','REST2_LR','REST2_RL'};
task = task_list{4};

% func_create_phys_kscript(subject, baseDir, 0.45, 3, 0.2);
[Ts,Fs,TR, trig,PPGlocs, HR, resp, cardiac,movRegr,resp_10,RVT,RF] = func_load_scan_kscript(subject,task,baseDir);
time = 0:Ts:(length(trig)-1)*Ts;
time_10 = 0:0.1:time(end);
Ts_10 = 0.1; Fs_10 = 10; t_win= 0 :Ts_10:60; N_win = length(t_win);


RV = zeros(size(resp_10));
N = length(resp_10);
for i = 2:N-1
    ind_1 = i-6*Fs_10;   ind_1 = max(1,ind_1);
    ind_2 = i+6*Fs_10;   ind_2 = min(ind_2, N);
    RV(i) = std(resp_10(ind_1:ind_2));
end
RV(1) = RV(2); RV(end) = RV(end-1);    RV = RV(:);

%     resp_s = smooth(resp_10,10*1.5) ;
%     RF = diff(resp_s); RF=[0;RF(:)]; RF = RF.^2;

% plotting
screenSize = get(0,'ScreenSize'); xL = screenSize(3); yL = screenSize(4);
set(gcf, 'Position', [1          82        1920        1282 ]);
ax1 = subplot(5,2,3);
plot(time_10,RV,'LineWidth',3)
% plot(time_10,RVT,'LineWidth',3)
title('Respiration volume (RV)')
ylabel(' a.u.')
%         ylim([-0.01 1.6])
xlabel('Time (s)')

subplot(ax1)
ax1.XGrid = 'on';
ax1.GridAlpha=0.7;
ax1.GridLineStyle='--';
ax1.FontSize = 17;
ax1.FontWeight = 'bold';
