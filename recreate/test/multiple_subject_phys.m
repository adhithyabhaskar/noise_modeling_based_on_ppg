clear;
addpath(genpath(pwd))
Fs = 400; Ts = 1/Fs; 
baseDir = 'data/';
subject = ['103818'];
task_list = {'REST1_LR','REST1_RL','REST2_LR','REST2_RL'};
task = task_list{4};

func_create_phys_kscript(subject, baseDir, 0.45, 3, 0.2);
[Ts,Fs,TR, trig,PPGlocs, HR, resp_10, cardiac,movRegr,~,~,~] = func_load_scan_kscript(subject,task,baseDir);

% figure;
% t_cardiac = 0:Ts:(length(trig)-1)*Ts;
% plot(t_cardiac, cardiac)

screenSize = get(0,'ScreenSize'); xL = screenSize(3); yL = screenSize(4);
% figure
set(gcf, 'Position', [1          82        2560        1282 ]);
%         set(gcf, 'Position', [0.1*xL 0.1*yL  0.8*xL 0.8*yL ]);
ax1 = subplot(5,2,1);
time_10 = 0:0.1:(length(HR)-1)*0.1;
plot(time_10, smooth(HR,30))
ylabel('HR (bpm)')
title(sprintf('Heart rate (HR; %2.0f %1.0f bpm )',mean(HR),std(HR)))
subplot(ax1)
ax1.XGrid = 'on';
ax1.GridAlpha=0.7;
ax1.GridLineStyle='--';
ax1.FontSize = 17;
ax1.FontWeight = 'bold';

% func_create_phys_kscript(subject, baseDir, 0.73, 7, 0.2);
[Ts,Fs,TR, trig,PPGlocs, HR, resp_10, cardiac,movRegr] = func_load_scan(subject,task,baseDir);

% figure;
% t_cardiac = 0:Ts:(length(trig)-1)*Ts;
% plot(t_cardiac, cardiac)

% figure;
% time_10 = 0:0.1:(length(HR)-1)*0.1;
% plot(time_10, HR)