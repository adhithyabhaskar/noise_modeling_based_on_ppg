clear; close all;
addpath(genpath(pwd))
set(0,'DefaultFigureWindowStyle','docked') 

%% Set Constants

load('sample_phys_voxel_S105014_R1LR.mat')
subjects = ['105014'];
data_dir = ['data/'];
Fs = 400; TR = 0.72;

%% Read from logs and normalize data

% subject from example (RETR_CPM_one_voxel.m)

log_task = func_normalize_raw_logs(subjects, data_dir);

% Only need the cardiac and resp for the first task: Rest1LR
log_resp_raw =  log_task{2,1}(:,2);
log_resp_norm =  log_task{3,1}(:,2);

log_cardiac_raw =  log_task{2,1}(:,3); %% Raw from logs
log_cardiac_norm =  log_task{3,1}(:,3);

log_trig =  log_task{2,1}(:,1); %% Raw from logs

[new_PPGlocs, log_cardiac_filt] = func_ppg_from_card(log_cardiac_norm, Fs);

[new_HR_10, ~] = func_hr_10_from_ppg(log_cardiac_filt, new_PPGlocs, Fs);
[new_HR_10_no_outlier, t_HR_10] = func_hr_10_from_ppg_no_outlier(log_cardiac_filt, new_PPGlocs, Fs);

%% Figure 1 - Compare outlier removal function and recreated vs. provided HR_10
tiledlayout(2,1)

ax1 = nexttile;
plot(t_HR_10, new_HR_10, '.:')
hold on
plot(t_HR_10, HR_10, '.:')
legend("New", "Old")
title("With outlier removal")
hold off;
xlim([7.3 72.2])
ylim([56.5 75.0])

ax2 = nexttile;
plot(t_HR_10, new_HR_10_no_outlier, '.:')
hold on
plot(t_HR_10, HR_10, '.:')
legend("New", "Old")
title("Without outlier removal")
hold off;
xlim([7.3 72.2])
ylim([56.5 75.0])

linkaxes([ax1 ax2],'xy')

%% Figure 2 - Compare PPGLocs values
figure
new_PPGlocs = transpose(new_PPGlocs(1:length(new_PPGlocs)-1));
plot(new_PPGlocs-PPGlocs)
title("New vs old PPGLocs")

%% Figure 3 - Compare PPGLocs stems with HR
figure
tiledlayout(2,1)
t = 0:length(log_cardiac_filt)-1;
t = t*1/400;

ax1 = nexttile;
plot(t, log_cardiac_filt)
hold on;
arr=ones(1,length(new_PPGlocs))*max(log_cardiac_filt);
stem(PPGlocs, arr)
hold off;
xlim([7.1 45.4])
ylim([-3.00 3.00])
title("Old PPGLocs")

ax2 = nexttile;
plot(t, log_cardiac_filt)
hold on;
arr=ones(1,length(new_PPGlocs))*max(log_cardiac_filt);
stem(new_PPGlocs, arr)
hold off;
xlim([7.1 45.4])
ylim([-3.00 3.00])
title("New PPGLocs")

linkaxes([ax1 ax2],'xy')

%% Figure 4 - Compare PPGLocs stems same axis
figure

t = 0:length(log_cardiac_filt)-1;
t = t*1/400;

plot(t, log_cardiac_filt, 'DisplayName','Filtered cardiac','color','#0072BD')
hold on;
arr=ones(1,length(PPGlocs))*max(log_cardiac_filt);
stem(PPGlocs, arr, 'DisplayName','Old PPGLocs','color',	'#D95319')
stem(new_PPGlocs, arr, 'DisplayName','New PPGLocs','color',	'#77AC30')
legend
hold off;
xlim([7.1 45.4])
ylim([-3.00 3.00])

title("Compare PPGLocs stems")

%% Figure 5 - Compare Trig values
close;
figure;
t = 0:length(log_cardiac_filt)-1;
arr = transpose(t*1/400);
stem(arr, log_trig)
hold on;
stem(arr, trig, Marker="+")
legend("log_trig", "given_trig")

