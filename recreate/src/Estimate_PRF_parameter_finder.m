
clear, clc, close all

addpath(genpath(pwd));
baseDir='data/';
fig_path = 'output/plots/';
log_path = ['logs/', 'Estimate_PRF_log_', datestr(clock,'YYYYmmdd_HHMM'), '.txt'];
% load([baseDir,'subject_list.mat'])
% subject_list = [subject_list_R3];
% subj_remove = {'199251','114924','100206'};
% for i = 1:length(subj_remove)
%     ind = find(subject_list == subj_remove{i} );  subject_list(ind) = [];
% end

%% create phys data from Physio log files for all scans

% subject_list = ["105014"];
% subject_list = ["103818"; "105014"];
subject_list = readmatrix("data/available_subjects.txt",OutputType="string");
nSubj = length(subject_list); nScans = nSubj*4;
task_list = {'REST1_LR','REST1_RL','REST2_LR','REST2_RL'};
% task_list = {'REST2_RL'};

if exist(fig_path,'file')~=7
    mkdir(fig_path); end

if exist('logs/','file')~=7
    mkdir('logs/'); end

fileID = fopen(log_path,'w');

save_path = 'Export/2020_02_01/Estimate_PRF_shift_0_RV_PartialCorr_v2/';  if exist(save_path,'file')~=7, mkdir(save_path); end

%%  1:  Load physiological variables (heart rate and respiration) and global signal (GS) from MAT-File

%  Set the following parameters !!

Ts = 1/400; 
% HPF_f = 0.7;  [filt_b,filt_a] = butter(2,HPF_f*2*Ts,'high');

TR = 0.72; 
HPF_f = 0.01;  [filt_b,filt_a] = butter(2,HPF_f*2*TR,'high');

volDel=20;
Ts_10 = 0.1; Fs_10 = 10; t_win= 0 :Ts_10:60; N_win = length(t_win);

overwrite = 0; % 0 to not overwrite GS.mat if it already exists for the scan

% Parameter testing section
num_params = 2+4+1+1; % 4 r-values to store, 1 for pass/fail flag, 1 for the optimal parameters
minPeak_all = transpose(0.45:0.05:0.90); % 0.45-0.90
filloutliers_ThresholdFactor_hr_all = transpose(2:1:15); % 3-15
[p,q] = meshgrid(minPeak_all, filloutliers_ThresholdFactor_hr_all);
parameter_space = [p(:) q(:)];
parameter_space_result= [zeros(nScans, length(parameter_space), num_params)];

% Models
r_all = zeros(nScans,4);
CRF_all = zeros(N_win,nScans);
BRF_all = zeros(N_win,nScans);
RRF_all = zeros(N_win,nScans);
FC_all = zeros(nScans, 4,4);
FC_partial_all = zeros(nScans,4,4);
M = 8;
%%  -----------

% Calculate GS.mat for all subjects/scans
for i=1:nSubj
    for j=1:length(task_list)
        func_print_and_file(fileID,'Subject: %s  (%d/%d);   Run: %d/%d \n',subject_list{i},i,nSubj,j,length(task_list));
        func_calc_gs(subject_list{i}, task_list{j}, baseDir, overwrite, fileID);
    end
end
func_print_and_file(fileID,"GS creation done \n")

tic
for c = 1:nScans

    s = ceil(c/4);        run = c - (s-1)*4;
    subject = char(subject_list(s,:));         task = char(task_list(run));
    func_print_and_file(fileID, 'Subject: %s     (%d/%d);   Run: %d/%d    \n',subject,s,nSubj,run,4)
    temp_best_parameters=zeros(1,3);

    GS = func_load_gs(subject, task, baseDir);

	for param=1:length(parameter_space)
         lastwarn(''); % clear warnings at start of loop
%         try     
                func_create_phys_kscript(subject, baseDir, parameter_space(param, 1), parameter_space(param, 2), 0.5, fileID);

                [Ts,Fs,TR,trig,PPGlocs,HR,~,cardiac,movRegr,resp_10,~,~] = func_load_scan_kscript(subject,task,baseDir);
                
                time = 0:Ts:(length(trig)-1)*Ts;
                time_10 = 0:0.1:time(end);
                ind_BOLD=find(trig==1);
                trig(ind_BOLD(1:volDel))=0;
                ind_BOLD=find(trig==1);
                timeMR=time(trig==1); timeMR = timeMR +TR/2;
                NV = length(timeMR);
                
                GS = filtfilt(filt_b,filt_a,GS) ;
                GS = zscore(GS);
                
                ind_BOLD_10 = zeros(NV,1);
                for i = 1:NV
                t = timeMR(i);        [val, loc] = min(abs(time_10-t));
                ind_BOLD_10(i) = loc;
                end
                
                % Extract cardiac
                indPPGlocs = zeros(size(PPGlocs));
                for i = 1 :length(PPGlocs)
                [val, loc] = min(abs(PPGlocs(i)-time));
                indPPGlocs(i) = loc;
                end
                pks = cardiac(indPPGlocs);
                cardiac = interp1([0,time(indPPGlocs),time(end)],[pks(1),pks',pks(end)],time);
                cardiac = interp1(time, cardiac, time_10);    cardiac = zscore(cardiac(:));

                % Extract RV
                RV = zeros(size(resp_10));
                N = length(resp_10);
                for i = 2:N-1
                ind_1 = i-6*Fs_10;   ind_1 = max(1,ind_1);
                ind_2 = i+6*Fs_10;   ind_2 = min(ind_2, N);
                RV(i) = std(resp_10(ind_1:ind_2));
                end
                RV(1) = RV(2); RV(end) = RV(end-1);    RV = RV(:);
                RF = RV;

                %% 2: Estimate PRF parameters
                        
                options = optimoptions('fmincon','Display','off','Algorithm','interior-point',...
                'UseParallel',false,'MaxIterations',100,'MaxFunctionEvaluations',3000,'OptimalityTolerance',1e-8);
                %     options = optimoptions('fmincon','Display','iter-detailed','Algorithm','interior-point',...
                %         'UseParallel',false,'MaxIterations',100,'MaxFunctionEvaluations',3000,'OptimalityTolerance',1e-8,'PlotFcn','optimplotfval');    % 'PlotFcn','optimplotfval'
                
                PRF_par = [  3.1    2.5   5.6    0.9    1.9   2.9   12.5    0.5    3.1    2.5   5.6    0.9  ]; PRF_par_0 = PRF_par;
                ub = PRF_par+3;
                lb = PRF_par-3; lb(lb<0)=0;
                
                h_train = @(P) func_PRF_w_ePPG_tmp(P,Ts_10,HR,RF,cardiac, ind_BOLD_10,GS,1,1:NV,1:NV,filt_b,filt_a);
                PRF_par = fmincon(h_train,PRF_par,[],[],[],[],lb,ub,[],options);
                h = @(P) func_PRF_w_ePPG_tmp(P,Ts_10,HR,RF,cardiac,ind_BOLD_10,GS,0,1:NV,1:NV,filt_b,filt_a);
                [obj_function,CRF_sc,RRF_sc,BRF_sc, r_PRF_sc,yPred, yPred_card, yPred_resp, yPred_PPG] = h(PRF_par);
                
                [warnmsg, msgid] = lastwarn;
                if ~strcmp(msgid,'MATLAB:rankDeficientMatrix')
                    CRF_all(:,c) = CRF_sc;
                    BRF_all(:,c) = BRF_sc;
                    RRF_all(:,c) = RRF_sc;       
                    r_all(c,:) = r_PRF_sc;
    
                    X = [yPred_resp, yPred_card, yPred_PPG,GS];
                    FC = corr(X);
                    
                    FC_partial = partialcorr(X);    
                    Part_RV_HR_PPG = partialcorr(yPred_resp,yPred_card,yPred_PPG);
                    FC_partial(1,2) = Part_RV_HR_PPG;     FC_partial(2,1) = Part_RV_HR_PPG; 
                    Part_RV_PPG_HR = partialcorr(yPred_resp,yPred_PPG,yPred_card);
                    FC_partial(1,3) = Part_RV_PPG_HR;     FC_partial(3,1) = Part_RV_PPG_HR; 
                    Part_HR_PPG_RV = partialcorr(yPred_card,yPred_PPG,yPred_resp);
                    FC_partial(2,3) = Part_HR_PPG_RV;     FC_partial(3,2) = Part_HR_PPG_RV; 
                    
                    FC_all(c,:,:) = FC;        
                    FC_partial_all(c,:,:) = FC_partial;
    
                    func_print_and_file(fileID, ' ----------------------------------------------- \n')

                    if strcmp(msgid,'MATLAB:rankDeficientMatrix')
                        func_print_and_file(fileID, 'Warning!: %3.2f and %3.2f  \n', parameter_space(param, 1), parameter_space(param, 2))
                        parameter_space_result(c, param,7) = 0; % 0 flag if rank deficient warning
                    else
                        func_print_and_file(fileID, 'Passed: %3.2f and %3.2f  \n', parameter_space(param, 1), parameter_space(param, 2))
                        parameter_space_result(c, param,7) = 1;
                    end
                    func_print_and_file(fileID, 'Correlation b/w GS and PRF output \n')
                    func_print_and_file(fileID, 'CRF (HR): %3.2f  \n',r_PRF_sc(2))
                    func_print_and_file(fileID, 'RRF (RF): %3.2f  \n',r_PRF_sc(3))
                    func_print_and_file(fileID, 'PPG (BP): %3.2f  \n',r_PRF_sc(4))
                    func_print_and_file(fileID, 'CRF & RRF (HR & RF): %3.2f  \n',r_PRF_sc(1))
    
                    parameter_space_result(c, param,1:2) = parameter_space(param,:);
                    parameter_space_result(c, param,3:6) = [r_PRF_sc(2), r_PRF_sc(3), r_PRF_sc(4), r_PRF_sc(1)];
    
                else
                parameter_space_result(c, param,1:2) = parameter_space(param,:);
                parameter_space_result(c, param,3:6) = [r_PRF_sc(2), r_PRF_sc(3), r_PRF_sc(4), r_PRF_sc(1)];
                func_print_and_file(fileID, 'Warning!: %3.2f and %3.2f  \n', parameter_space(param, 1), parameter_space(param, 2))
                parameter_space_result(c, param,7) = 0; % 0 flag if rank deficient warning
                end
    end

    % finding parameters that result in the maximum mean r-value
    avg_rvalues = mean(squeeze(parameter_space_result(c,:,3:6)),2);
    [~, max_row] = max(avg_rvalues);
    parameter_space_result(c,max_row,8) = 2;

    func_estimate_prf_plot(fig_path, subject, task, CRF_sc, Ts_10, time_10, HR, timeMR, GS, yPred_card, ...
        r_PRF_sc, cardiac, yPred_PPG, RV, yPred_resp, yPred, BRF_sc, RRF_sc, ...
        parameter_space_result(c,max_row,1), parameter_space_result(c,max_row,2));
    
end
func_print_and_file(fileID, ' ----------------------------------------------- \n')
func_print_and_file(fileID, 'Time elapsed (minutes): %3.1f  \n', toc/60)

save('data/workspace.mat');
save([save_path, 'shift_0_RV_PartialCorr'],'r_all','CRF_all','BRF_all','RRF_all','FC_all','FC_partial_all')

%%  ---------------------------------

% plot(r_all)
% 
% mean(r_all)
% 
% CM = squeeze(mean(FC_all))
% imagesc(CM)
% 
% 
% CM_partial = squeeze(mean(FC_partial_all))
% imagesc(CM_partial)
% 
% 
% mean(r_all_wos)
% mean(r_all_ws)
% 
% x1 = r_all_wos(:,1);
% x2 = r_all_ws(:,1);
% 
% x1_sbj = reshape(x1',[4 100]);  x1_sbj = mean(x1_sbj)';
% x2_sbj = reshape(x2',[4 100]);  x2_sbj = mean(x2_sbj)';
% 
% [ttest_p ttest_h] = ttest(x1_sbj,x2_sbj)


%% Figure 7 (a)
%%  --------------------

% nScans = 400;
% 
% CRF_mean = zeros(N_win,1);
% BRF_mean = zeros(N_win,1);
% RRF_mean = zeros(N_win,1);
% for c = 1:nScans
%     x = CRF_all(:,c)*r_all(c,2);
%     CRF_mean = CRF_mean + x/nScans;
%     
%     x = BRF_all(:,c)*r_all(c,4);
%     BRF_mean = BRF_mean + x/nScans;
%     
%     x = RRF_all(:,c)*r_all(c,3);
%     RRF_mean = RRF_mean + x/nScans;
% end
% 
% figure('position', [  940   703   555   420])
% ax = plot(t_win,[CRF_mean, BRF_mean, RRF_mean],'linewidth',3), grid on
% 
% ax = gca;
% ax.GridLineStyle = '--';
% ax.Box = 'off';
% ax.XAxisLocation = 'origin';
% 
% legend('CRF','PARF','RRF');  legend boxoff
% 
% xlim([0 50]), ylim([-0.5 0.5])
% xlabel('Time (s)')
% ylabel('Amplitude (a.u.)')
% 






