%%  3: Plot output of PRF model (timeseries and PRF curves)

%  Set the following parameters !!
smoothPar = 1;
fontTitle = 20;
fontLabels = 8;
fontTxt = 16;
lineW = 3;
yl1 = -5.3; yl2 = 5.5;

% -----------------------------------------

% load(['data/workspace.mat']);
fig_path = 'output/plots/';
if ~exist(fig_path,'file')~=7, mkdir(fig_path); end

t_IR = 0:Ts_10:(length(CRF_sc)-1)*Ts_10;

screenSize = get(0,'ScreenSize'); xL = screenSize(3); yL = screenSize(4);
figure('visible','off')
set(gcf, 'Position', [1          82        2560        1282 ]);
%         set(gcf, 'Position', [0.1*xL 0.1*yL  0.8*xL 0.8*yL ]);
        
ax1 = subplot(5,2,1);
plot(time_10,smooth(HR,30),'LineWidth',lineW)
ylabel('HR (bpm)')
title(sprintf('Heart rate (HR; %2.0f+-%1.0f bpm )',mean(HR),std(HR)))
%         title('Heart rate (HR)')

ax2 = subplot(5,2,2);
h1=plot(timeMR,smooth(GS,smoothPar),'LineWidth',lineW); hold on
%         h2=plot(time_10,HR_conv,'LineWidth', lineW);
h2=plot(timeMR,yPred_card,'LineWidth', lineW);
%         legend([h1,h2],'Global signal', 'X_{HR}')  ,      legend('boxoff')
title('BOLD fluctuations due to changes in HR')
text(60, 4,  sprintf('r=%3.2f  ',  r_PRF_sc(2)) ,'FontSize',fontTxt,'FontWeight','bold')
ylabel(' a.u.')
ylim([yl1, yl2])

ax3 = subplot(5,2,3);
plot(time_10,cardiac,'LineWidth',lineW)
ylabel(' a.u.')
title(sprintf('Photoplethysmographic amplitude (PPG-Amp)'))

ax4 = subplot(5,2,4);
h1=plot(timeMR,smooth(GS,smoothPar),'LineWidth',lineW); hold on
h2=plot(timeMR,yPred_PPG,'LineWidth', lineW);
%         legend([h1,h2],'Global signal', 'X_{HR}')  ,      legend('boxoff')
title('BOLD fluctuations due to changes in PPG-Amp')
text(60, 4,  sprintf('r=%3.2f  ',  r_PRF_sc(4)) ,'FontSize',fontTxt,'FontWeight','bold')
ylabel(' a.u.')
ylim([yl1, yl2])

ax5 = subplot(5,2,5);
plot(time_10,RV,'LineWidth',lineW), hold on
title('Respiration volume (RV)')
ylabel(' a.u.')
%         ylim([-0.01 1.6])
xlabel('Time (s)')

ax6 = subplot(5,2,6);
h1 = plot(timeMR,smooth(GS,smoothPar),'LineWidth',lineW); hold on
h2 = plot(timeMR,yPred_resp,'LineWidth',lineW);
title('BOLD fluctuations due to changes in RV')
text(60, 4,  sprintf('r=%3.2f  ',  r_PRF_sc(3)) ,'FontSize',fontTxt,'FontWeight','bold')
%         legend([h1,h2],'Global signal', 'X_{HR}')  ,      legend('boxoff')
ylabel(' a.u.')
ylim([yl1, yl2])        

%%Figure 8 (g)
%         if 1
ax7 = subplot(5,2,[7,9]);      
%         figure('position', [  940   703   800   420])
% 
plot(t_IR,CRF_sc,'LineWidth',4), hold on
plot(t_IR,BRF_sc,'LineWidth',4),
plot(t_IR,RRF_sc,'LineWidth',4), grid on
title('Physiological response functions (PRFs) ')
xlabel('Time (s)'), ylabel('Amplitude (a.u.)')
legend('CRF','PARF','RRF','Location','northeastoutside')
xlim([0 60]), ylim([-1.2 1.2])
%         end

ax8 = subplot(5,2,[8,10]);
h1=plot(timeMR,smooth(GS,smoothPar),'LineWidth',lineW); hold on
h2=plot(timeMR,yPred,'LineWidth',lineW);
%         title('Full model')
text(60, 4,  sprintf('r=%3.2f  ',  r_PRF_sc(1)) ,'FontSize',fontTxt,'FontWeight','bold')
ylabel(' a.u.')
legend([h1,h2],'Global signal','Full model')
ylim([yl1, yl2])
legend('boxoff')
xlabel('Time (s)')

linkaxes([ax1,ax2,ax3, ax4, ax5,ax6,ax8],'x')
xlim([timeMR(1) timeMR(end)])

% ax_list = [ax1,ax2,ax3, ax4,ax5,ax6,ax7,ax8];
ax_list = [ax1,ax2,ax3, ax4,ax5,ax6,ax8];

for ax=ax_list
    subplot(ax)
    ax.XGrid = 'on';
    ax.GridAlpha=0.7;
    ax.GridLineStyle='--';
    ax.FontSize = 17;
    ax.FontWeight = 'bold';
end

%         savefig([save_path, 'PRF_sc_',subject,'_scan_',num2str(run),'.fig'])
        saveas(gcf, [save_path, 'PRF_sc_',subject,'_scan_',num2str(run),'.png'])
