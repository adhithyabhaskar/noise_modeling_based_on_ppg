function [PPGLocs, filt_cardiac] = func_ppg_from_card(cardiac, Fs)
%FUNC_PPG_AND_HR_FROM_CARD Summary of this function goes here
%   Detailed explanation goes here
    [A,B] = butter(1,[0.3 10]/(Fs/2));
    filt_cardiac = filtfilt(A,B,cardiac);

    min_peak_dist = 0.5*Fs; % up to 0.9 depending on subjects's avg HR
    [~,PPGLocs] = findpeaks(filt_cardiac,'MinPeakDistance',min_peak_dist);
    PPGLocs = PPGLocs/Fs; % digital to time (s)
end

