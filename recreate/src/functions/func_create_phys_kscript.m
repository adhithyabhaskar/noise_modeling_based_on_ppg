function [phys_path] = func_create_phys_kscript(subject, data_dir, minPeak, filloutliers_ThresholdFactor_hr, filloutliers_ThresholdFactor_resp, fileID)
%FUNC_CREATE_PHYS_KSCRIPT Summary of this function goes here
%   Uses the Preprocess_Phys.m script provided in the PRF_estimation submodule
%   Input: Single subject name, Location of subject data, minPeak and filloutliers_ThresholdFactors for heart rate & resp outlier correction (more instructions in Preprocess_Phys.m)
%   Output: Directory of saved phys.mat (data/subject/physio_kscript/REST1_LR_phys.mat)

task_list = {'REST1_LR','REST1_RL','REST2_LR','REST2_RL'};
% Constants
Fs = 400; TR = 0.72;

% Read from logs and normalize data
log_task = func_normalize_raw_logs(subject, data_dir, fileID);
for i=1:length(task_list)
    trig_log =  log_task{3,i}(:,1);
    resp =  log_task{3,i}(:,2);
    cardiac =  log_task{3,i}(:,3);

    % Create timeline at the frequency sampling Fs = 400 Hz that the recordings
    % were acquried

    N = length(resp);
    Fs = 400; Ts=1/Fs;  time=0:Ts:(N-1)*Ts;

    % 2: Find the triggers (or timepoints) that the fMRI volumes were acquired 
    % The triggers in each scan of the resting-state fMRI data in the HCP
    % should sum up to 1200

    tmp = diff(trig_log);
    loc = find(tmp>0);
    trig = zeros(size(trig_log));
    trig(1) = 1;
    trig(loc) = 1;

    % 3: Preprocess the cardiac signal and extract the heart rate (HR)
    filloutliers_window = 30*Fs;     % given in number of samples
    Fs_10 = 10; Ts_10 = 1/Fs_10; time_10 = time(1):Ts_10:time(end);

    f1 = 0.3; f2 = 10;     [filt_b,filt_a] = butter(2,[f1,f2]/(Fs/2));
    cardiac_filt = filtfilt(filt_b,filt_a,cardiac);
    [pks,PPGlocs] = findpeaks(cardiac_filt,time,'MinPeakDistance',minPeak);

    HR = 60./diff(PPGlocs);
    time_HR = [time(1),(PPGlocs(2:end)-PPGlocs(1:end-1))/2+PPGlocs(1:end-1),time(end)];
    HR_raw_400 = interp1(time_HR,[HR(1),HR,HR(end)],time);
    HR_filloutl_400 = filloutliers(HR_raw_400,'linear','movmedian',filloutliers_window,'ThresholdFactor',filloutliers_ThresholdFactor_hr);
    HR = interp1(time,HR_filloutl_400,time_10); HR=HR(:);

    % 4: Preprocess the respiratory signal
    filloutliers_window = 0.3*Fs;     % given in number of samples

    f1=0.01; f2=5; [filt_b,filt_a] = butter(2,[f1,f2]/(Fs/2));
    resp = detrend(resp,'linear');
    resp1 = filloutliers(resp,'linear','movmedian',filloutliers_window,'ThresholdFactor',filloutliers_ThresholdFactor_resp);
    resp2 = zscore(filter(filt_b,filt_a,resp1));

    % 5: Extract breathing-related variables
    resp = resp2;
    resp_10 = interp1(time,resp,time_10);

    [pks,loc] = findpeaks(resp,time,'MinPeakDistance',2,'MinPeakHeight',0.2);
    respUpp = interp1([0,loc,time(end)],[pks(1),pks',pks(end)],time_10);

    [pks,loc] = findpeaks(-resp,time,'MinPeakDistance',2,'MinPeakHeight',0.2); pks=-pks;
    respLow = interp1([0,loc,time(end)],[pks(1),pks',pks(end)],time_10);

    BR = 60./diff(loc);
    time_BR = [time(1),(loc(2:end)-loc(1:end-1))/2+loc(1:end-1),time(end)];
    BR = interp1(time_BR,[BR(1),BR,BR(end)],time_10);

    RVT = ((respUpp-respLow).*BR)';
    resp_s = smooth(resp_10,10*1.5) ;
    RF = diff(resp_s); RF=[0;RF(:)]; RF = RF.^2;

    % trig = func_trig_adjust(trig_log);
    % [PPGlocs, filt_cardiac] = func_ppg_from_card(cardiac, Fs);
    % [HR_10, t_HR_10] = func_hr_10_from_ppg(filt_cardiac, PPGlocs, Fs);
    save_dir = join([data_dir, subject, '/MNINonLinear/physio/'], "");
    phys_path = join([save_dir, task_list{i}, '_phys_kscript.mat'],"");
    if exist(save_dir,'file')~=7
        mkdir(save_dir);
    end

    save(phys_path,'trig','time','Fs','TR','PPGlocs','HR','Fs_10','resp','resp_10','BR','RVT','RF','cardiac_filt');
end

