function [save_path] = func_calc_gs(subject, task, baseDir, overwrite, fileID)
%FUNC_CALC_GS Summary of this function goes here
%   Detailed explanation goes here
tic
save_path = join([baseDir, subject, '/MNINonLinear/physio/', task, '_GS.mat'],"");

if exist(save_path,'file')==2 && overwrite==0
    func_print_and_file(fileID, 'GS.mat for subject %s task %s already exists \n', subject, task)
    return
end

volDel=20;

baseDir_subj = [baseDir, subject,'/MNINonLinear/Results/'];
filepath_nii=[baseDir_subj,'rfMRI_',task,'.nii.gz'];
filepath_mask=[baseDir_subj,'/brainmask_fs.2.nii.gz'];

nii=load_untouch_nii(filepath_nii); img=single(nii.img);
nii_mask=load_untouch_nii(filepath_mask); imgMask=nii_mask.img;

disp('Loading data - done'); fprintf('Time elapsed : %3.1f  minutes \n ', toc/60);

disp('Extracting img_col')
TR = 0.72;
HPF_f = 0.008;    
[filt_b,filt_a] = butter(2,HPF_f*2*TR,'high');
% Filter values if Signal Processing Toolbox not available
% filt_a = [1,-1.948828742744042,0.950105726761822];
% filt_b = [0.974733617376466,-1.949467234752932,0.974733617376466];

img(:,:,:,1:volDel) = [];
[NX,NY,NZ,NV]=size(img);
NVX=0;
CordAll=zeros(NX*NY*NZ,3);
for x=1:NX
    for y=1:NY
        for z=1:NZ
            if imgMask(x,y,z)>0.5
                voxel=reshape(img(x,y,z,:),NV,1);
                voxelMean = mean(voxel);
                if voxelMean>100
                    NVX=NVX+1;
                    CordAll(NVX,1)=x;
                    CordAll(NVX,2)=y;
                    CordAll(NVX,3)=z;
                    voxel=single(voxel); voxel=detrend(voxel,'linear');
                    img(x,y,z,:)=voxel+voxelMean;
                end
            end
        end
    end
end
CordAll=CordAll(1:NVX,:);

img_col=zeros(NV,NVX);
for vox=1:NVX
    x=CordAll(vox,1);    y=CordAll(vox,2);    z=CordAll(vox,3);
    img_col(:,vox)=reshape(img(x,y,z,:),NV,1);
end
img_col = ft_filtfilt(filt_b,filt_a,img_col);

disp('Extracting  img_col  - done'), fprintf('Time elapsed: %3.1f minutes \n ', toc/60)

GS = mean(img_col,2);
save(save_path, 'GS');

end

