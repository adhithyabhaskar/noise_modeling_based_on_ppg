function [Ts,Fs,TR, trig,PPGlocs, HR_10, resp, cardiac,movRegr] = func_load_scan(subject, task, baseDir)
%FUNC_LOAD_SCAN Summary of this function goes here
%   Detailed explanation goes here

filepath_movRegr=[baseDir, subject,'/MNINonLinear/Results/Movement_Regressors_dt.txt'];
movRegr=load(filepath_movRegr);  movRegr=[movRegr, movRegr.^2];

filepath_MRacq=[baseDir, subject,'/MNINonLinear/physio/', task, '_phys.mat'];
load(filepath_MRacq,"Fs","trig","TR","filt_cardiac", "resp","PPGlocs","HR_10"); Ts = 1/Fs;
cardiac=filt_cardiac;

end

