function [] = func_create_phys_all_avail_subj(data_dir, sj_list_dir)
%FUNC_CREATE_PHYS_ALL_AVAIL_SUBJ Script to create Phys.mat for all subjects in the available_subjects.txt list
% Creates both phys.mat and the kscript phys.mat (Preprocess_phys.m script from the PRF Estimation repository)
% Inputs data_dir: location of subject data
%        sj_list_dir: location of available_subjects.txt 
subject_list = readmatrix([sj_list_dir, 'available_subjects.txt'],OutputType="string");
for i = 1:size(subject_list)
    subject = subject_list(i);
    fprintf("Processing subject %s\n", subject)
    func_create_phys(subject, data_dir);
    func_create_phys_kscript(subject, data_dir, 0.73, 7, 0.2);
end
fprintf("Created phys.mat and phys_kscript.mat for %d subjects\n", length(subject_list))


