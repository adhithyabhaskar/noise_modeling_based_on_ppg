function [phys_path] = func_create_phys(subject, data_dir)
%FUNC_CREATE_PHYS Summary of this function goes here
%   Input: Single subject name, Location of subject data
%   Output: Directory of saved phys.mat (data/subject/physio/REST1_LR_phys.mat)

task_list = {'REST1_LR','REST1_RL','REST2_LR','REST2_RL'};
% Constants
Fs = 400; TR = 0.72;

% Read from logs and normalize data
log_task = func_normalize_raw_logs(subject, data_dir);
for i=1:length(task_list)
    trig_log =  log_task{3,i}(:,1);
    resp =  log_task{3,i}(:,2);
    cardiac =  log_task{3,i}(:,3);

    trig = func_trig_adjust(trig_log);
    [PPGlocs, filt_cardiac] = func_ppg_from_card(cardiac, Fs);
    [HR_10, t_HR_10] = func_hr_10_from_ppg(filt_cardiac, PPGlocs, Fs);

    save_dir = join(['data/', subject, '/MNINonLinear/physio/'], "");
    phys_path = join([save_dir, task_list{i}, '_phys.mat'],"");
    if exist(save_dir,'file')~=7
        mkdir(save_dir);
    end

    % Don't have 'voxel' yet
    % Transposing HR_10 and PPGlocs to match given phys.mat sample
    % Also removing the element of PPGLocs to more closely match given PPGlocs
    HR_10 = transpose(HR_10);
    PPGlocs = transpose(PPGlocs(2:end)); 
    save(phys_path, "Fs", "HR_10", "t_HR_10", "PPGlocs", "TR", "cardiac", "filt_cardiac", "resp", "trig")
end

