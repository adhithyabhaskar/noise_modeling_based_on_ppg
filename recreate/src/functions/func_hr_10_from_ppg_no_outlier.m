function [HR_10, t_resample] = func_hr_10_from_ppg_no_outlier(cardiac, PPGlocs, Fs)
%FUNC_CREATE_HR_FROM_CARD Summary of this function goes here
%   Detailed explanation goes here
    diff_peaks = diff(PPGlocs);
    HR = (60*(1./diff_peaks));

    t_resample = 0:(1/10):(length(cardiac)/Fs);
    % using extrap fills in the missing NaN's at the beginning and end of
    % interpolated HR_10
    HR_10 = interp1(PPGlocs, [HR; HR(end)], t_resample, 'linear', 'extrap');
end

