function [adjusted_trig] = func_trig_adjust(trig)
%FUNC_TRIG_ADJUST Summary of this function goes here
%   Detailed explanation goes here
    adjusted_trig = zeros(length(trig),1);
    adjusted_trig(diff(trig)>0) = 1;
    adjusted_trig(1) = 1;
end

