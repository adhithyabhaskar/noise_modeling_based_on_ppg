clear; clc; close all;

baseDir='data/';
subject_list = ["103818"];
% func_create_phys_all_avail_subj(baseDir, baseDir);
% subject_list = readmatrix("data/available_subjects.txt",OutputType="string");
nSubj = length(subject_list); nScans = nSubj*4;

task_list = {'REST1_LR','REST1_RL','REST2_LR','REST2_RL'};

tic
for c = 1 : nScans
    s = ceil(c/4);        run = c - (s-1)*4;
    subject = char(subject_list(s,:));         task = char(task_list(run));
    fprintf('Subject: %s     (%d/%d);   Task: %s    \n',subject,s,nSubj,task)
    
    func_calc_gs(subject, task, baseDir);
%     plot( timeMR, GS), title('Global signal (GS)'), xlim([min(timeMR),max(timeMR)])
end
